import { Body, Controller, Del, Get, Inject, Param, Post, Provide, Put, Query } from "@midwayjs/decorator";
import { MenuService } from "../service/menu";
import { MenuDto } from "../dto/menu";
import { R } from "../../../common/common.error";

@Provide()
@Controller('/menu', {description: '菜单管理'})
export class MenuController {
    @Inject()
    menuService: MenuService;

    @Post('/', {description: '创建一个菜单'})
    async create(@Body() data: MenuDto) {
        return await this.menuService.createMenu(data);
    }

    @Del('/:id', {description: '删除一个菜单'})
    async remove(@Param('id') id: string) {
        if(!id) {
            throw R.validateError('id不能为空')
        }
        return await this.menuService.removeMenu(id);
    }

    @Put('/', {description: '更新菜单'})
    async update(@Body() data: MenuDto) {
        return await this.menuService.editMenu(data);
    }

    @Get('/page', {description: '分页查询菜单'})
    async page(@Query('page') page: number, @Query('size') size: number) {
        return await this.menuService.page(page, size);
    }

    @Get('/list', {description: '查询全量菜单'})
    async list() {
        return await this.menuService.list();
    }
}