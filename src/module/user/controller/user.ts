import { Controller, Inject, Post } from "@midwayjs/decorator";
import { UserService } from "../service/user";

@Controller('/user', {description: 'users management'})
export class UserController {
    @Inject()
    userService: UserService;

    @Post('/add', {description: 'add'})
    async addUser() {
        const user = await this.userService.addUser()
        return {
            success: true,
            message: 'ok',
            data: user
        }
    }
}