import { Provide } from "@midwayjs/decorator";
import { InjectEntityModel } from "@midwayjs/typeorm";
import { UserEntity } from "../entity/user";
import { Repository } from "typeorm";

@Provide()
export class UserService {
    @InjectEntityModel(UserEntity)
    userModel: Repository<UserEntity>;

    // async getUser() {
    //     return {
    //         uid: 
    //     }
    // }

    async addUser() {
        let record = new UserEntity();
        record = this.userModel.merge(record, {
            id: '1111',
            userName: 'aaa',
            age: 18,
            nickName: 'bbb',
        })

        try {
            const created = await this.userModel.save(record);
            return created;
        } catch (e) {
            console.log(e)
        }
    }
}