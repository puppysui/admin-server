import { MidwayConfig } from '@midwayjs/core';

export default {
  // use for cookie sign key, should change to your own and keep security
  keys: '1705484981268_101',
  koa: {
    port: 7002,
  },
  // orms: {
  //   type: 'mysql',
  //   host: '43.139.229.15',
  //   port: 3306,
  //   username: 'root',
  //   password: 'root',
  //   database: 'fluxy-admin',
  // },
  typeorm: {
    dataSource: {
      default: {
        type: 'mysql',
        host: '43.139.229.15',
        port: 3306,
        username: 'root',
        password: 'root',
        database: 'admin-db',
        synchronize: true, // 如果第一次使用，不存在表，有同步的需求可以写 true，注意会丢数据
        logging: true,
        entities: ['**/entity/*{.ts,.js}'], // 扫描entity文件夹
      }
    }
  }
} as MidwayConfig;
