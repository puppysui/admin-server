import { Column, Entity } from "typeorm";
import { BaseEntity } from "../../../common/base.entity";

@Entity('my_user')
export class UserEntity extends BaseEntity {
    @Column({comment: 'username'})
    userName: string;
    @Column({comment: 'nickname'})
    nickName: string;
    @Column({comment: 'phone number'})
    phoneNumber: string;
    @Column({comment: 'email'})
    email: string;
    @Column({comment: 'password'})
    password: string;
    @Column({comment: 'women: 0, men: 1', nullable: true})
    sex?: number;
    @Column({comment: 'age'})
    age?: number;
}