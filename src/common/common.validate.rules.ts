import { RuleType } from "@midwayjs/validate";

// 字符串
export const string = RuleType.string();
// 字符串不能为空
export const requiredString = string.required();


// 数字
export const number = RuleType.number();
// 数字不能为空
export const requiredNumber = number.required();

// 布尔值
export const bool = RuleType.bool()