import { Rule } from "@midwayjs/validate";
import { BaseDTO } from "../../../common/base.dto";
import { MenuEntity } from "../entity/menu";
import { bool, number, requiredString, string } from "../../../common/common.validate.rules";

export class MenuDto extends BaseDTO<MenuEntity> {
    @Rule(string.allow(null))
    parentId?: string;
    @Rule(string.allow(null))
    name?: string;
    // @Rule(string.allow(null))
    // icon?: string;
    // @Rule(string.allow(null))
    // type?: number;
    @Rule(string.allow(null))
    route?: string;
    // @Rule(string.allow(null))
    // filePath?: string;
    // @Rule(number.allow(null))
    // orderNumber?: number;
    // @Rule(string.allow(null))
    // url?: string;
    // @Rule(bool.allow(null))
    // show?: boolean;
    // @Rule(string.allow(null))
    // authCode?: string;
    // @Rule(string.allow(null))
    // pageSetting?: string;
    // @Rule(RuleType.array().items(getSchema(ApiDTO)).allow(null))
    // apis?: ApiDTO[];
}