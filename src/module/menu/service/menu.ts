import { Provide } from "@midwayjs/decorator";
import { BaseService } from "../../../common/base.service";
import { MenuEntity } from "../entity/menu";
import { DataSource, FindOptionsOrder, FindOptionsWhere, Repository } from "typeorm";
import { InjectDataSource, InjectEntityModel } from "@midwayjs/typeorm";
import { MenuDto } from "../dto/menu";
import { R } from "../../../common/common.error";

@Provide()
export class MenuService extends BaseService<MenuEntity> {
    @InjectEntityModel(MenuEntity)
    menuModel: Repository<MenuEntity>;

    @InjectDataSource()
    defaultDataSource: DataSource;

    getModel(): Repository<MenuEntity> {
        return this.menuModel;
    }

    async createMenu(data: MenuDto) {
        if (data.route && (await this.menuModel.countBy({route: data.route})) > 0) {
            // throw R.error('路由不能重复')
        }

        const entity = data.toEntity();
        await this.defaultDataSource.transaction(async manager => {
            await manager.save(MenuEntity, entity);
        })

        return {...entity};
    }

    async editMenu(data: MenuDto) {
        const entity = data.toEntity();

        await this.defaultDataSource.transaction(async manager => {
            await manager
            .createQueryBuilder()
            .update(MenuEntity)
            .set({
                name: data.name,
                route: data.route,
            })
            .where('id = :id', {id: entity.id})
            .execute();

        })
        
        return {...entity}
    }

    async page2() {

    }

    async removeMenu(id: string) {
        return await this.menuModel
        .createQueryBuilder()
        .delete()
        .where('id = :id', {id})
        .execute();
    }

    async page(
        page: number,
        pageSize: number,
        where?: FindOptionsWhere<MenuEntity>
    ) {
        const order: FindOptionsOrder<MenuEntity> = {id: 'ASC'}
        const [data, total] = await this.menuModel.findAndCount({
            where,
            order,
            skip: page * pageSize,
            take: pageSize,
        })

        if (!data.length) return {data: [], total: 0}

        return {data, total}
    }
}