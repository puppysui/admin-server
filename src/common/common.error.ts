import { MidwayError, httpError } from '@midwayjs/core';
import { MidwayValidationError } from '@midwayjs/validate';

export class R {
    static error(message: string) {
        return new MidwayError(message)
    }

    // 校验报错
    static validateError(message: string) {
        return new MidwayValidationError(message, 422, null);
    }

    // 权限报错
    static unauthorizedError(message: string) {
        return new httpError.UnauthorizedError(message);
    }

    // 禁用访问报错
    static forbiddenError(message: string) {
        return new httpError.ForbiddenError(message);
    }
}